﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Login : MonoBehaviour {
    public InputField InputUsername;
    public InputField InputPassword;
    public Text TextStatus;

    public void ButtonLogin () {
        StartCoroutine(API.Request(API.LOGIN, (WWWForm form) => {
            form.AddField("username", InputUsername.text);
            form.AddField("password", InputPassword.text);

            TextStatus.text = "Waiting to Login...";

            return new Dictionary<string, string>();
        }, (WWW www) => {
            Debug.Log("Login Sukses");
            TextStatus.text = "Login Sukses";

            // Parsing to object
            Response.LOGIN_SUCCESS obj = JsonUtility.FromJson<Response.LOGIN_SUCCESS>(www.text);
            User.Inject(obj.name, obj.username, obj.token);

            SceneManager.LoadScene(Config.Scenes["MAINMENU"]);

            return 0;
        }, (WWW www) => { 
            Debug.Log("Login Failed " + www.text);

            Response.MESSAGE obj = JsonUtility.FromJson<Response.MESSAGE>(www.text);

            TextStatus.text = obj.message;

            return 1;
        }));
	}
}
